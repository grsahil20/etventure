require 'flickraw'

require_relative './models/collage'
require_relative './lib/dictionary'
require_relative './lib/file_download_util'
require_relative './lib/flickr_keyword_search'

FlickRaw.api_key = ENV['FLICKR_API_KEY'] || 'ce044d70f5d6d29388cef49e0bfab5c5'
FlickRaw.shared_secret = ENV['FLICKR_SHARED_SECRET'] || '04f7633426d4893c'

class EtventureCollageApp
  attr_accessor :keywords, :validated_hash

  def initialize(output_name:, input_keywords: [])
    @input_keywords = input_keywords
    @output_name = output_name
    @search_client = FlickRaw::Flickr.new
  end

  def run
    validate_keywords
    add_missing_random_keywords
    sorted_keywords
    download_images
    create_collage
    clear_images
  end

  private

  def validate_keywords
    @validated_hash = {}
    p 'received '+ @input_keywords.count.to_s + " keywords"
    return @validated_hash if @input_keywords == []
    @input_keywords.each do |input_keyword|
      break if  validated_keys.count == 10
      if url = FlickrKeywordSearch.new(keyword: input_keyword, search_client: @search_client).get_image_url
        @validated_hash[input_keyword] = url
      end
    end
    @validated_hash
  end

  def add_missing_random_keywords
    return if @validated_hash.keys.count > 9
    (10 - @validated_hash.keys.count).times do
      keyword = (Dictionary::SEARCH_WORDS - @validated_hash.keys).sample
      if url = FlickrKeywordSearch.new(keyword: keyword, search_client: @search_client).get_image_url
        @validated_hash[keyword] = url
      end
    end
  end

  def validated_keys
    @validated_hash.keys || []
  end

  def sorted_keywords
    @validated_keywords = validated_keys[0..9]
  end

  def download_images
    sorted_keywords.each_with_index do |validated_keyword, index|
      FileDownloadUtil.new(file_name: "#{@output_name}_#{index}.png", image_url: @validated_hash[validated_keyword]).save
    end
  end

  def clear_images
    sorted_keywords.each_with_index do |validated_keyword, index|
      path_to_file = "#{@output_name}_#{index}.png"
      p "deleting " +  path_to_file
      File.delete(path_to_file)
    end
  end

  def create_collage
    Collage.new(output_name: @output_name).save
  end
end
