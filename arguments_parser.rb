class ArgumentsParser
  def initialize(arguments)
    @arguments = arguments
  end

  def build_options
    @options ||= parse_arguments
  end

  def parse_arguments
    options = {}
    opt_parser = OptionParser.new do |opts|
      options[:input_keywords] = []
      opts.on('-k', "--keywords x,y,z", Array, "Example 'cat,dog,hello,space'") do |keywords|
        options[:input_keywords] = keywords.map{|k| k.strip }
      end

      options[:output_name] = Time.now.to_i.to_s
      opts.on("-o", "--output=NAME", "Name to output file") do |n|
        options[:output_name] = n
      end

      opts.on_tail("-h", "--help", "Show this message") do
        puts opts
        exit
      end
    end
    opt_parser.parse!(ARGV)
    options
  end
end

