require 'flickraw'

class FlickrKeywordSearch
  def initialize(keyword:, search_client:)
    @search_client = search_client || FlickRaw::Flickr.new
    @keyword = keyword
  end

  def get_image_url
    return @url if @url
    p "searching for " + @keyword + ".........."
    if flckr_image = @search_client.photos.search(text: @keyword, sort: :relevance).first
      flckr_image_info = @search_client.photos.getInfo(photo_id: flckr_image.id, secret: flckr_image.secret)
      @url ||= FlickRaw.url(flckr_image_info)
    else
      p "no search result found  for " + @keyword + " dropping .........."
      nil
    end
  end
end
