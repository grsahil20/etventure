require 'open-uri'

class FileDownloadUtil
  def initialize(file_name:, image_url:)
    @file_name = file_name
    @image_url = image_url
  end

  def save
    p "downloading image " + @file_name + " url : " + @image_url + ".........."
    open(@file_name, 'wb') do |file|
      file << open(@image_url).read
    end
  end
end
