require 'rmagick'

class Collage
  def initialize(output_name:, count: 9)
    @output_name = output_name
    @count = count
  end

  def save
    # http://stackoverflow.com/questions/7629167/creating-a-collage-from-a-collection-of-images-in-ruby
    bg = nil
    bg = Magick::Image.new(1000, 400)
    (0..@count).to_a.each do |index|
        img = Magick::ImageList.new("#{@output_name}_#{index}.png").resize_to_fill(200, 200)
        left = (index % 5)*200
        top = (index.to_i / 5)*200
        bg.composite!(img, left, top, Magick::OverCompositeOp)
    end

    bg.write("#{@output_name}.png")
  end
end

# bg = Magick::Image.new(500, 200)
# img = Magick::ImageList.new('1490158647_cat.png').resize_to_fit(500, 100)
# img2 = Magick::ImageList.new('1490158647_dog.png').resize_to_fit(500, 100)

# bg = bg.composite(img, 0, 0, Magick::OverCompositeOp)
# bg = bg.composite(img2, 0, 100, Magick::OverCompositeOp)
# bg.write('collage2.png')
